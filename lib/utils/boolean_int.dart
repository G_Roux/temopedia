int booleanToInt(bool val) => val ? 1 : 0;

bool intToBool(int val) => val == 1 ? true : val == 0 ? false : null;
